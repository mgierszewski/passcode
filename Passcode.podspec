Pod::Spec.new do |s|
	s.name         	= "Passcode"
	s.version      	= "1.1.0"
	s.summary      	= "Swift framework for setting up in-app passcode feature."
	s.license     	= "MIT"
	s.platform     	= :ios, "9.3"
	s.homepage		= "futuremind.com"
	s.author		= { "Maciej Gierszewski" => "m.gierszewski@futuremind.com" }
	s.source       	= { :git => "https://bitbucket.org/mgierszewski/passcode" }
	s.module_name  = 'Passcode'
    s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.0' }
	s.source_files	= "Passcode/**/*.{h,swift}"
	s.resources 	= "Passcode/Resources/*.{storyboard,strings}"
end
