//
//  ViewController.m
//  PasscodeTestApp
//
//  Created by Maciek Gierszewski on 04/12/2017.
//  Copyright © 2017 Maciek Gierszewski. All rights reserved.
//

#import "ViewController.h"
#import <Passcode/Passcode.h>

@interface ViewController ()
@property (nonatomic, strong) IBOutlet UILabel *biometricVeriticationLabel;
@property (nonatomic, strong) IBOutlet UISwitch *biometricVeriticationSwitch;
@property (nonatomic, strong) IBOutlet UISegmentedControl *appearanceSegmentedControl;

@property (nonatomic, strong) IBOutlet UIButton *setupButton;
@property (nonatomic, strong) IBOutlet UIButton *deleteButton;
@property (nonatomic, strong) IBOutlet UIButton *promptButton;

- (IBAction)setupPasscode:(id)sender;
- (IBAction)deletePasscode:(id)sender;
- (IBAction)promptForPasscode:(id)sender;

- (IBAction)changeBiometricVerification:(UISwitch *)aSwitch;
- (IBAction)changeColor:(UISegmentedControl *)segmentedControl;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PasscodeManager *manager = [PasscodeManager shared];
    BiometricVerificationType biometricVerificationType = manager.availableBiometricVerificationType;
    
    self.promptButton.enabled = manager.isPasscodeSet;
    self.deleteButton.enabled = manager.isPasscodeSet;
    
    self.appearanceSegmentedControl.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:kPasscodeAppearance];
    self.biometricVeriticationSwitch.enabled = biometricVerificationType != BiometricVerificationTypeNone;
    self.biometricVeriticationSwitch.on = manager.isBiometricVerificationEnabled;

    switch (biometricVerificationType) {
        case BiometricVerificationTypeNone:
            self.biometricVeriticationLabel.text = @"None";
            break;
        case BiometricVerificationTypeFaceID:
            self.biometricVeriticationLabel.text = @"Face ID";
            break;
        case BiometricVerificationTypeTouchID:
            self.biometricVeriticationLabel.text = @"Touch ID";
            break;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)setupPasscode:(id)sender
{
    __weak __typeof__(self) weakSelf = self;
    [[PasscodeManager shared] setupPasscodeFromViewController:self completionHandler:^(BOOL success) {
        weakSelf.promptButton.enabled = [[PasscodeManager shared] isPasscodeSet];
        weakSelf.deleteButton.enabled = [[PasscodeManager shared] isPasscodeSet];
    }];
}

- (void)deletePasscode:(id)sender
{
    __weak __typeof__(self) weakSelf = self;
    [[PasscodeManager shared] deletePasscodeFromViewController:self completionHandler:^(BOOL success) {
        weakSelf.promptButton.enabled = [[PasscodeManager shared] isPasscodeSet];
        weakSelf.deleteButton.enabled = [[PasscodeManager shared] isPasscodeSet];
    }];
}

- (void)promptForPasscode:(id)sender
{
    [[PasscodeManager shared] waitForPasscodeWithCompletionHandler:^{
    }];
}

- (IBAction)unwindFromModal:(UIStoryboardSegue *)segue
{
}

- (void)changeBiometricVerification:(UISwitch *)aSwitch
{
    PasscodeManager *manager = [PasscodeManager shared];
    [manager setBiometricVerificationEnabled:aSwitch.on completion:^(BOOL success, NSError *error) {
        aSwitch.on = success;
    }];
}

- (void)changeColor:(UISegmentedControl *)segmentedControl
{
    PasscodeManager *manager = [PasscodeManager shared];
    manager.appearance = (PasscodeAppearance)segmentedControl.selectedSegmentIndex;
    
    [[NSUserDefaults standardUserDefaults] setInteger:segmentedControl.selectedSegmentIndex forKey:kPasscodeAppearance];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
