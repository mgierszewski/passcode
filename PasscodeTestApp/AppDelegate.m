//
//  AppDelegate.m
//  PasscodeTestApp
//
//  Created by Maciek Gierszewski on 04/12/2017.
//  Copyright © 2017 Maciek Gierszewski. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <Passcode/Passcode.h>

@interface AppDelegate ()
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    PasscodeManager *manager = [PasscodeManager shared];
    manager.backgroundColor = [UIColor groupTableViewBackgroundColor];
    manager.foregroundColor = [UIColor labelColor];
    manager.titleImage = [UIImage imageNamed:@"logo"];
    manager.appearance = [[NSUserDefaults standardUserDefaults] integerForKey:kPasscodeAppearance];
    
    [manager applicationDidFinishLaunching];
    [manager waitForPasscodeWithCompletionHandler:^{
        NSLog(@"passcode entered; application:didFinishLaunchingWithOptions:");
    }];
    
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    PasscodeManager *manager = [PasscodeManager shared];
    [manager waitForPasscodeWithCompletionHandler:^{
        NSLog(@"passcode entered; applicationWillEnterForeground:");
    }];
}

@end
