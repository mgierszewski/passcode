//
//  main.m
//  PasscodeTestApp
//
//  Created by Maciek Gierszewski on 04/12/2017.
//  Copyright © 2017 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
