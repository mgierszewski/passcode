//
//  UIBezierPath.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 20/12/2017.
// 

import Foundation
import AVKit

let eraseIconDefaultSize = CGSize(width: 46.0, height: 34.0)

enum EraseIconMode {
    case filled
    case outlined
}

extension UIBezierPath {
    convenience init(eraseIconWithMode mode: EraseIconMode, rect: CGRect = CGRect(origin: .zero, size: eraseIconDefaultSize)) {
        let defaultLineWidth = CGFloat(3.0)

        let shapeLineWidth = mode == .filled ? CGFloat(0.0) : defaultLineWidth
        let defaultSize = eraseIconDefaultSize
        let crossSize = CGSize(width: 12.0, height: 12.0)
        let crossCenter = CGPoint(x: 28.0, y: 17.0)
        let cornerRadius = CGFloat(7.0)
        let pointCornerRadius = CGFloat(3.0)

        let a = cornerRadius / (1.0 + sqrt(2.0))
        let b = pointCornerRadius / sqrt(2.0)
        let c = pointCornerRadius * (sqrt(2.0) - 1.0)

        self.init()

        // draw the main shape
        move(to: CGPoint(x: shapeLineWidth/2.0 + b - c, y: defaultSize.height/2.0 - b))

        // top left
        addArc(withCenter: CGPoint(x: defaultSize.height/2.0 + a - c, y: cornerRadius + shapeLineWidth/2.0), radius: cornerRadius, startAngle: -CGFloat.pi * 0.75, endAngle: -CGFloat.pi * 0.5, clockwise: true)

        // top right
        addArc(withCenter: CGPoint(x: defaultSize.width - cornerRadius - shapeLineWidth/2.0, y: cornerRadius + shapeLineWidth/2.0), radius: cornerRadius, startAngle: -CGFloat.pi * 0.5, endAngle: 0.0, clockwise: true)

        // bottom right
        addArc(withCenter: CGPoint(x: defaultSize.width - cornerRadius - shapeLineWidth/2.0, y: defaultSize.height - shapeLineWidth/2.0 - cornerRadius), radius: cornerRadius, startAngle: 0.0, endAngle: CGFloat.pi * 0.5, clockwise: true)

        // bottom left
        addArc(withCenter: CGPoint(x: defaultSize.height/2.0 + a - c, y: defaultSize.height - shapeLineWidth/2.0 - cornerRadius), radius: cornerRadius, startAngle: CGFloat.pi * 0.5, endAngle: CGFloat.pi * 0.75, clockwise: true)

        // back to the middle
        addArc(withCenter: CGPoint(x: shapeLineWidth/2.0 + pointCornerRadius, y: defaultSize.height/2.0), radius: pointCornerRadius, startAngle: CGFloat.pi * 0.75, endAngle: CGFloat.pi * 1.25, clockwise: true)

        // the cross
        switch mode {
        case .filled:

            // draw "empty" path
            let crossPath = UIBezierPath()
            let crossLineWidth = defaultLineWidth * sqrt(2.0)/2.0

            // top left
            crossPath.move(to: CGPoint(x: crossCenter.x - crossLineWidth, y: crossCenter.y))
            crossPath.addLine(to: CGPoint(x: crossCenter.x - crossSize.width/2.0 - crossLineWidth, y: crossCenter.y - crossSize.height/2.0))
            crossPath.addLine(to: CGPoint(x: crossCenter.x - crossSize.width/2.0, y: crossCenter.y - crossSize.height/2.0 - crossLineWidth))
            crossPath.addLine(to: CGPoint(x: crossCenter.x, y: crossCenter.y - crossLineWidth))

            // top right
            crossPath.addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0, y: crossCenter.y - crossSize.height/2.0 - crossLineWidth))
            crossPath.addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0 + crossLineWidth, y: crossCenter.y - crossSize.height/2.0))
            crossPath.addLine(to: CGPoint(x: crossCenter.x + crossLineWidth, y: crossCenter.y))

            // bottom right
            crossPath.addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0 + crossLineWidth, y: crossCenter.y + crossSize.height/2.0))
            crossPath.addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0, y: crossCenter.y + crossSize.height/2.0 + crossLineWidth))
            crossPath.addLine(to: CGPoint(x: crossCenter.x, y: crossCenter.y + crossLineWidth))

            // bottom left
            crossPath.addLine(to: CGPoint(x: crossCenter.x - crossSize.width/2.0, y: crossCenter.y + crossSize.height/2.0 + crossLineWidth))
            crossPath.addLine(to: CGPoint(x: crossCenter.x - crossSize.width/2.0 - crossLineWidth, y: crossCenter.y + crossSize.height/2.0))
            crossPath.addLine(to: CGPoint(x: crossCenter.x - crossLineWidth, y: crossCenter.y))

            crossPath.close()
            append(crossPath.reversing())

        case .outlined:

            // draw just the line
            move(to: CGPoint(x: crossCenter.x - crossSize.width/2.0, y: crossCenter.y - crossSize.height/2.0))
            addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0, y: crossCenter.y + crossSize.height/2.0))

            move(to: CGPoint(x: crossCenter.x - crossSize.width/2.0, y: crossCenter.y + crossSize.height/2.0))
            addLine(to: CGPoint(x: crossCenter.x + crossSize.width/2.0, y: crossCenter.y - crossSize.height/2.0))
        }

        // draw
        let outputRect = AVMakeRect(aspectRatio: defaultSize, insideRect: rect)
        let scale = outputRect.width/defaultSize.width
        let transform = CGAffineTransform(translationX: rect.width/2.0 - outputRect.width/2.0, y: rect.height/2.0 - outputRect.height/2.0).scaledBy(x: scale, y: scale)

        lineWidth = shapeLineWidth * scale
        lineJoinStyle = .round
        lineCapStyle = .round

        apply(transform)
    }
}
