//
//  UIApplication.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 25/12/2017.
//

import Foundation

extension UIApplication {
    func waitForActiveApplicationState(_ handler: @escaping () -> Void) {
        if applicationState == .active {
            handler()
            return
        }

        var observer: Any?
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: OperationQueue.current) { _ in
            if let observer = observer {
                NotificationCenter.default.removeObserver(observer)
            }

            handler()
        }
    }
}
