//
//  String.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 20/12/2017.
// 

import Foundation

let setupPasscodeText = "Setup passcode".localizedString
let changePasscodeText = "Change passcode".localizedString
let deletePasscodeText = "Delete passcode".localizedString

let enterYourPasscodeText = "Enter your passcode.".localizedString
let enterPasscodeText = "Enter a new passcode.".localizedString
let confirmPasscodeText = "Verify your new passcode.".localizedString
let passcodesDoNotMatchText = "Passcodes do not match.".localizedString
let incorrectPasscodeText = "Incorrect passcode.".localizedString
let biometricVerificationReasonText = "Unlock the app.".localizedString

extension String {
    var localizedString: String {
        let bundle = Bundle(for: PasscodeManager.self)
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: "")
    }
}
