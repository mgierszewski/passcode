//
//

import Foundation
import AVFoundation

extension UIImage {
    convenience init?(withSize size: CGSize, fillColor: UIColor? = nil, borderColor: UIColor? = nil, shadowColor: UIColor? = nil, shadowOffset: UIOffset = .zero, cornerRadius: CGFloat = 0.0, lineWidth: CGFloat = 0.0) {
        let imageSize = size.height * size.width == 0 ? CGSize(width: lineWidth + 2 * cornerRadius + 1.0, height: lineWidth + 2 * cornerRadius + 1.0) : CGSize(width: size.width, height: size.height)
        let scale = UIScreen.main.scale

        let shapeInsets = UIEdgeInsets(top: max(0.0, -shadowOffset.vertical), left: max(0.0, -shadowOffset.horizontal), bottom: max(0.0, shadowOffset.vertical), right: max(0.0, shadowOffset.horizontal))
        let shapeRect = CGRect(origin: .zero, size: imageSize).inset(by: shapeInsets)

        let shapePath = UIBezierPath(roundedRect: shapeRect.insetBy(dx: lineWidth/2.0, dy: lineWidth/2.0), cornerRadius: cornerRadius)
        shapePath.lineCapStyle = .square
        shapePath.lineJoinStyle = .miter
        shapePath.lineWidth = lineWidth

        let shadowPath = UIBezierPath(roundedRect: shapeRect.offsetBy(dx: shadowOffset.horizontal, dy: shadowOffset.vertical), cornerRadius: cornerRadius)

        let shadowMaskPath = UIBezierPath(rect: .infinite)
        shadowMaskPath.append(shapePath.reversing())

        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        if let context = UIGraphicsGetCurrentContext() {
            if let shadowColor = shadowColor {
                shadowMaskPath.addClip()

                shadowColor.setFill()
                shadowPath.fill()

                context.resetClip()
            }

            if let fillColor = fillColor {
                fillColor.setFill()
                shapePath.fill()
            }

            if let borderColor = borderColor {
                borderColor.setStroke()
                shapePath.stroke()
            }
        }
        let imageRef = UIGraphicsGetImageFromCurrentImageContext()?.cgImage
        UIGraphicsEndImageContext()

        if let imageRef = imageRef {
            self.init(cgImage: imageRef, scale: scale, orientation: .up)
        } else {
            return nil
        }
    }

    convenience init?(eraseIconWithStrokeColor strokeColor: UIColor?) {
        let rect = CGRect(x: 0.0, y: 0.0, width: 23, height: 17)
        let bezierPath = UIBezierPath(eraseIconWithMode: .outlined, rect: rect)
        let scale = UIScreen.main.scale

        UIGraphicsBeginImageContextWithOptions(rect.size, false, scale)
        if UIGraphicsGetCurrentContext() != nil {
            if let strokeColor = strokeColor {
                strokeColor.setStroke()
                bezierPath.stroke()
            }
        }
        let imageRef = UIGraphicsGetImageFromCurrentImageContext()?.cgImage
        UIGraphicsEndImageContext()

        if let imageRef = imageRef {
            self.init(cgImage: imageRef, scale: scale, orientation: .up)
        } else {
            return nil
        }
    }

    convenience init?(highlightedEraseIconWithFillColor fillColor: UIColor?) {
        let rect = CGRect(x: 0.0, y: 0.0, width: 23, height: 17)
        let bezierPath = UIBezierPath(eraseIconWithMode: .filled, rect: rect)
        let scale = UIScreen.main.scale

        UIGraphicsBeginImageContextWithOptions(rect.size, false, scale)
        if UIGraphicsGetCurrentContext() != nil {
            if let fillColor = fillColor {
                fillColor.setFill()
                bezierPath.fill()
            }
        }
        let imageRef = UIGraphicsGetImageFromCurrentImageContext()?.cgImage
        UIGraphicsEndImageContext()

        if let imageRef = imageRef {
            self.init(cgImage: imageRef, scale: scale, orientation: .up)
        } else {
            return nil
        }
    }
}
