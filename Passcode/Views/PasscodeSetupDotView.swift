//
//  PasscodeSetupDotView.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 19/12/2017.
// 

import UIKit

class PasscodeSetupDotView: PasscodeDotView {
    override func update() {
        let lineWidth: CGFloat = 1.0
        guard frame.width > lineWidth && frame.height > lineWidth else {
            return
        }

        let bezierPath: UIBezierPath

        switch state {
        case .filled:
            shapeLayer?.fillColor = tintColor.cgColor
            shapeLayer?.strokeColor = nil
            shapeLayer?.lineWidth = 0.0

            bezierPath = UIBezierPath(ovalIn: bounds)

        case .empty:
            if #available(iOS 11.0, *) {
                shapeLayer?.fillColor = nil
                shapeLayer?.strokeColor = tintColor.cgColor
                shapeLayer?.lineWidth = 1.0

                bezierPath = UIBezierPath(ovalIn: bounds.insetBy(dx: lineWidth/2.0, dy: lineWidth/2.0))

            } else {
                bezierPath = UIBezierPath(rect: bounds.insetBy(dx: 0.0, dy: floor((bounds.height - 4.0)/2.0)))
            }
        }

        let transition = CATransition()
        transition.duration = 0.2

        shapeLayer?.path = bezierPath.cgPath
        shapeLayer?.add(transition, forKey: "path")
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: 18.0, height: 18.0)
    }
}
