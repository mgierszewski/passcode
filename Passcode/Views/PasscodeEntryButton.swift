//
//  PasscodeButton.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

class PasscodeEntryButton: PasscodeButton {

    // MARK: update
    override func update() {
        let backgroundImage = UIImage(withSize: bounds.size, fillColor: appearance.entryButtonBackgroundColor, borderColor: appearance.entryButtonBorderColor, cornerRadius: bounds.size.width/2.0, lineWidth: 2.0)
        setBackgroundImage(backgroundImage, for: .normal)

        let highlightedBackgroundImage = UIImage(withSize: bounds.size, fillColor: appearance.entryButtonHighlightedBackgroundColor, cornerRadius: bounds.size.width/2.0)
        setBackgroundImage(highlightedBackgroundImage, for: .highlighted)
    }

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 84.0, height: 84.0)
    }
}
