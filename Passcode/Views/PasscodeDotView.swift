//
//  PasscodeDotView.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit

enum PasscodeDotViewState {
    case empty
    case filled
}

class PasscodeDotView: UIView {
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }

    var state: PasscodeDotViewState = .empty {
        didSet {
            update()
        }
    }

    var shapeLayer: CAShapeLayer? {
        return layer as? CAShapeLayer
    }

    func update() {
    }

    override var tintColor: UIColor! {
        didSet {
            update()
        }
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: 15.0, height: 15.0)
    }

    override func updateConstraints() {
        setContentCompressionResistancePriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .horizontal)

        super.updateConstraints()
    }

    public override var bounds: CGRect {
        didSet {
            update()
        }
    }
}
