//
//  PasscodeSetupButton.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 19/12/2017.
// 

import Foundation

class PasscodeSetupButton: PasscodeButton {

    // MARK: update
    override func update() {
        let cornerRadius: CGFloat
        if #available(iOS 11.0, *) {
            cornerRadius = 4.0
        } else {
            cornerRadius = 0.0
        }

        let backgroundImage = UIImage(withSize: bounds.size, fillColor: appearance.setupButtonBackgroundColor, shadowColor: appearance.setupButtonShadowColor, shadowOffset: appearance.setupButtonShadowOffset, cornerRadius: cornerRadius)
        setBackgroundImage(backgroundImage, for: .normal)

        let highlightedBackgroundImage = UIImage(withSize: bounds.size, fillColor: appearance.setupButtonHighlightedBackgroundColor, shadowColor: appearance.setupButtonShadowColor, shadowOffset: appearance.setupButtonShadowOffset, cornerRadius: cornerRadius)
        setBackgroundImage(highlightedBackgroundImage, for: .highlighted)
    }
}
