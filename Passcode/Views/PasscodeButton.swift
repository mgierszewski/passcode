//
//  PasscodeButtn.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit
import AudioToolbox

class PasscodeButton: UIButton {

    // MARK: public vars
    public let number: Int

    public var appearance: PasscodeAppearance = .default {
        didSet {
            update()
        }
    }

    init(withNumber number: Int) {
        self.number = number
        super.init(frame: .zero)

        addTarget(self, action: #selector(onTouchDown), for: .touchDown)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: actions
    @objc func onTouchDown() {
        AudioServicesPlaySystemSound(1104)

        UIDevice.current.playInputClick()
    }

    // MARK: update
    func update() {
    }

    public override var bounds: CGRect {
        didSet {
            update()
        }
    }
}

extension PasscodeButton: UIInputViewAudioFeedback {
    var enableInputClicksWhenVisible: Bool {
        return true
    }
}
