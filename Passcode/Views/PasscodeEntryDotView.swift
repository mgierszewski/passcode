//
//  PasscodeDotView.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit

class PasscodeEntryDotView: PasscodeDotView {
    override func update() {
        let lineWidth: CGFloat = 2.0
        guard frame.width > lineWidth && frame.height > lineWidth else {
            return
        }

        let bezierPath: UIBezierPath

        switch state {
        case .filled:
            shapeLayer?.strokeColor = nil
            shapeLayer?.lineWidth = 0.0
            shapeLayer?.fillColor = tintColor.cgColor

            bezierPath = UIBezierPath(ovalIn: bounds)

        case .empty:
            shapeLayer?.strokeColor = tintColor.cgColor
            shapeLayer?.lineWidth = lineWidth
            shapeLayer?.fillColor = nil

            bezierPath = UIBezierPath(ovalIn: bounds.insetBy(dx: lineWidth/2.0, dy: lineWidth/2.0))
        }

        let transition = CATransition()
        transition.duration = 0.2

        shapeLayer?.path = bezierPath.cgPath
        shapeLayer?.add(transition, forKey: "fillColor")
    }
}
