//
//  PasscodeLabel.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit

class PasscodeButtonLabel: UIView {
    private let numberLabel = UILabel()
    private let lettersLabel = UILabel()
    private let stackView = UIStackView()
    private let spacerView = UIView()

    public var appearance: PasscodeAppearance = .default {
        didSet {
            updateNumberLabel()
            updateLettersLabel()
        }
    }

    public var number: Int = 0 {
        didSet {
            updateNumberLabel()
        }
    }

    public var letters: String = "" {
        didSet {
            updateLettersLabel()
        }
    }

    var numberFont: UIFont = .systemFont(ofSize: 36.0) {
        didSet {
            updateNumberLabel()
        }
    }

    var lettersFont: UIFont = .boldSystemFont(ofSize: 9.0) {
        didSet {
            updateLettersLabel()
        }
    }

    private var paragraphStyle: NSParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center

        return paragraphStyle
    }

    private func updateNumberLabel() {
        numberLabel.attributedText = NSAttributedString(string: "\(number)", attributes: [.foregroundColor: appearance.buttonForegroundColor,
                                                                                          .font: numberFont,
                                                                                          .paragraphStyle: paragraphStyle])
    }

    private func updateLettersLabel() {
        lettersLabel.attributedText = NSAttributedString(string: "\(letters)", attributes: [.foregroundColor: appearance.buttonForegroundColor,
                                                                                            .font: lettersFont,
                                                                                            .paragraphStyle: paragraphStyle,
                                                                                            .kern: 2.0])
    }

    // MARK: initializers

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(withNumber number: Int, letters: String) {
        super.init(frame: .zero)
        super.isUserInteractionEnabled = false
        super.backgroundColor = .clear

        self.number = number
        self.letters = letters

        updateNumberLabel()
        updateLettersLabel()

        spacerView.backgroundColor = .clear

        stackView.addArrangedSubview(spacerView)
        stackView.addArrangedSubview(numberLabel)
        stackView.addArrangedSubview(lettersLabel)
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.isUserInteractionEnabled = false
        stackView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(stackView)
        NSLayoutConstraint.activate([
            spacerView.widthAnchor.constraint(equalTo: lettersLabel.widthAnchor),
            spacerView.heightAnchor.constraint(equalToConstant: CGFloat.leastNonzeroMagnitude),

            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
    }

    // MARK: layout

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        stackView.axis = traitCollection.verticalSizeClass == .compact ? .horizontal : .vertical
        stackView.spacing = traitCollection.verticalSizeClass == .compact ? 4.0 : -2.0
    }
}
