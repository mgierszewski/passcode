// 
//  PasscodeManagerError
//
//  Created by Maciek Gierszewski on 05/12/2019.
//

import Foundation

enum PasscodeManagerError: Error {
    case biometricVerificationUnavailable
    case faceIDUsageDescriptionUnavailable
}

extension PasscodeManagerError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .biometricVerificationUnavailable:
            return "Biometric verification unavailable"
        case .faceIDUsageDescriptionUnavailable:
            return "NSFaceIDUsageDescription is not set in the .plist file"
        }
    }
}

extension PasscodeManagerError: CustomNSError {
}
