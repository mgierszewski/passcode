//
//  Keychain.swift
//
//  Created by Maciek Gierszewski on 29.01.2016.
//

import Foundation

class Keychain {
	static let shared = Keychain()

	private var appIdentifier: String

	private init() {
		appIdentifier = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "Default"
	}

	private func searchDictionary(forIdentifier identifier: String) -> [String: AnyObject] {
		var dictionary = [String: AnyObject]()

		// Specify we are using a password (rather than a certificate, internet password, etc).
		dictionary[kSecClass as String] = kSecClassGenericPassword

		// Uniquely identify this keychain accessor.
		dictionary[kSecAttrService as String] = appIdentifier as AnyObject?

		// Uniquely identify the account who will be accessing the keychain.
		if let encodedIdentifier = identifier.data(using: String.Encoding.utf8) {
			dictionary[kSecAttrAccount as String] = encodedIdentifier as AnyObject?
		}

		return dictionary
	}

	func string(forKey key: String) -> String? {
		var searchDictionary = self.searchDictionary(forIdentifier: key)
		searchDictionary[kSecMatchLimit as String] = kSecMatchLimitOne
		searchDictionary[kSecReturnData as String] = true as AnyObject?

		var result: AnyObject?
		let status = SecItemCopyMatching(searchDictionary as CFDictionary, &result)

        if status == noErr, let data = result as? Data, let string = String(data: data, encoding: .utf8) {
            return string
        }

		return nil
	}

	func set(_ string: String?, forKey key: String) -> Bool {
		let valueAlreadyExists = self.string(forKey: key) != nil
		var searchDictionary = self.searchDictionary(forIdentifier: key)

		if let value = string, let valueData = value.data(using: String.Encoding.utf8) {
			if valueAlreadyExists {
				// update
				let updateDictionary = [kSecValueData as String: valueData]

				let status = SecItemUpdate(searchDictionary as CFDictionary, updateDictionary as CFDictionary)
				return status == errSecSuccess
			} else {
				// add
				searchDictionary[kSecValueData as String] = valueData as AnyObject?

				let status = SecItemAdd(searchDictionary as CFDictionary, nil)
				return status == errSecSuccess
			}
		} else if valueAlreadyExists {
			// delete
			let status = SecItemDelete(searchDictionary as CFDictionary)
			return status == errSecSuccess
		}

		return true
	}

	subscript(key: String) -> String? {
		get {
			return string(forKey: key)
		}
		set(newValue) {
			_ = set(newValue, forKey: key)
		}
	}
}

private let passcodeKey = "2acafa8d-52da-4653-9fc8-e7da3f3f8bc9"
private let biometricVerificationKey = "40e656c1-1394-4417-80ce-54b4b07784b9"

extension Keychain {
    var isBiometricVerificationEnabled: Bool {
        get {
            return string(forKey: biometricVerificationKey) != nil
        }
        set {
            if newValue {
                _ = set("true", forKey: biometricVerificationKey)
            } else {
                _ = set(nil, forKey: biometricVerificationKey)
            }
        }
    }

    var passcode: [Int]? {
        get {
            guard let passcodeString = string(forKey: passcodeKey) else {
                return nil
            }
            return passcodeString
                .map { Int(String($0)) ?? 0 }
        }
        set {
            if let newValue = newValue {
                let passcodeString = newValue
                    .map { "\($0)" }
                    .joined()

                _ = set(passcodeString, forKey: passcodeKey)
            } else {
                _ = set(nil, forKey: passcodeKey)
            }
        }
    }
}
