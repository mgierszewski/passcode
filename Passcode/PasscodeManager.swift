//
//  PasscodeManager.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import Foundation
import LocalAuthentication

@objc public enum BiometricVerificationType: Int {
    case none
    case touchID
    case faceID
}

extension BiometricVerificationType: CustomStringConvertible {
    public var description: String {
        switch self {
        case .none: return "None"
        case .touchID: return "Touch ID"
        case .faceID: return "Face ID"
        }
    }
}

@objc public class PasscodeManager: NSObject {
    @objc public static let shared = PasscodeManager()

    private var keychain = Keychain.shared
    private var window: UIWindow?

    override private init() {
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidFinishLaunching(_:)), name: UIApplication.didFinishLaunchingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive(_:)), name: UIApplication.willResignActiveNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: private

    @objc private func applicationDidFinishLaunching(_ notification: Notification) {
        preparePasscodeWindow()
        presentWindow()
    }

    @objc private func applicationDidBecomeActive(_ notification: Notification) {
        // destroy the window if it wasn't presented
        if let window = window, window.isHidden {
            destroyWindow()
        }
    }

    @objc private func applicationWillResignActive(_ notification: Notification) {
        // prepare passcode window
        preparePasscodeWindow()
    }

    @objc private func applicationDidEnterBackground(_ notification: Notification) {
        // present the window if it wasn't presented
        presentWindow()
    }

    private func changePasscode(fromViewController sourceViewController: UIViewController, setupNewPasscode: Bool, completionHandler: @escaping (Bool) -> Void) {
        let bundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Passcode", bundle: bundle)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PasscodeSetupViewController") as? PasscodeSetupViewController else {
            return
        }

        viewController.setupNewPasscode = setupNewPasscode
        viewController.backgroundColor = backgroundColor
        viewController.foregroundColor = foregroundColor
        viewController.appearance = appearance
        viewController.completionHandler = completionHandler

        let navigationController = UINavigationController(navigationBarClass: PasscodeSetupNavigationBar.self, toolbarClass: nil)
        navigationController.modalPresentationStyle = .formSheet
        navigationController.setViewControllers([viewController], animated: false)

        sourceViewController.present(navigationController, animated: true, completion: nil)
    }

    private func presentWindow(withViewController viewController: PasscodeEntryViewController) {
        window = UIWindow()
        window?.backgroundColor = .clear
        window?.windowLevel = UIWindow.Level.statusBar - 1
        window?.rootViewController = viewController

        if #available(iOS 13.0, *) {
            switch appearance {
            case .light:
                window?.overrideUserInterfaceStyle = .light
            case .dark:
                window?.overrideUserInterfaceStyle = .dark
            }
        }
    }

    private func destroyWindow() {
        window = nil
    }

    private func presentWindow() {
        if let window = window, window.isHidden {
            window.makeKeyAndVisible()
        }
    }

    private func preparePasscodeWindow() {
        guard keychain.passcode != nil && window == nil else {
            return
        }

        let bundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Passcode", bundle: bundle)

        // obtain the VC
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PasscodeEntryViewController") as? PasscodeEntryViewController else {
            return
        }

        viewController.titleImage = titleImage
        viewController.appearance = appearance
        viewController.addCompletionHandler(forKey: "f35c1ed7-a08d-43d6-8795-c0d8ef8ee4e5") { [weak self] in
            self?.destroyWindow()
        }

        window = UIWindow()
        window?.backgroundColor = .clear
        window?.windowLevel = UIWindow.Level.statusBar - 1
        window?.rootViewController = viewController
    }

    // MARK: public

    @objc public var appearance: PasscodeAppearance = .default
    @objc public var foregroundColor: UIColor = .black
    @objc public var backgroundColor: UIColor = .groupTableViewBackground
    @objc public var titleImage: UIImage?
    @objc public var defaultPasscode: String?

    @objc public var isPasscodeSet: Bool {
        return keychain.passcode != nil
    }

    @objc public var isBiometricVerificationEnabled: Bool {
        return keychain.isBiometricVerificationEnabled
    }

    @objc public func setBiometricVerificationEnabled(_ enabled: Bool, completion: @escaping (Bool, Error?) -> Void) {
        if enabled {
            let context = LAContext()

            var error: NSError?
            guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
                completion(false, PasscodeManagerError.biometricVerificationUnavailable)
                return
            }

            if #available(iOS 11.0, *) {
                if context.biometryType == .faceID, Bundle.main.object(forInfoDictionaryKey: "NSFaceIDUsageDescription") == nil {
                    completion(false, PasscodeManagerError.faceIDUsageDescriptionUnavailable)
                    return
                }
            }

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: biometricVerificationReasonText) { [weak keychain] (success, error) in
                keychain?.isBiometricVerificationEnabled = success

                completion(success, error)
            }

        } else {
            keychain.isBiometricVerificationEnabled = false
            completion(false, nil)
        }
    }

    @objc public var availableBiometricVerificationType: BiometricVerificationType {
        let context = LAContext()

        var error: NSError?
        let canEvaluatePolicy = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)

        if canEvaluatePolicy {
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .faceID:
                    return .faceID
                case .touchID:
                    return .touchID
                default:
                    return .none
                }
            }
            return .touchID
        }

        return .none
    }

    @objc public func applicationDidFinishLaunching() {

        // reset passcode during first init
        let firstSetupCompleteKey = "8ec6941e-92b7-4d45-88fa-0ed175b5c947"
        let firstSetupComplete = UserDefaults.standard.bool(forKey: firstSetupCompleteKey)
        if firstSetupComplete == false {
            keychain.passcode = defaultPasscode?.map { Int(String($0)) ?? 0 }
            keychain.isBiometricVerificationEnabled = false

            UserDefaults.standard.set(true, forKey: firstSetupCompleteKey)
            UserDefaults.standard.synchronize()
        }
    }

    @objc public func setupPasscode(fromViewController sourceViewController: UIViewController, completionHandler: @escaping (Bool) -> Void) {
        changePasscode(fromViewController: sourceViewController, setupNewPasscode: true, completionHandler: completionHandler)
    }

    @objc public func deletePasscode(fromViewController sourceViewController: UIViewController, completionHandler: @escaping (Bool) -> Void) {
        guard keychain.passcode != nil else {
            completionHandler(true)
            return
        }

        changePasscode(fromViewController: sourceViewController, setupNewPasscode: false, completionHandler: completionHandler)
    }

    @objc public func waitForPasscode(withCompletionHandler completionHandler: @escaping () -> Void) {
        guard keychain.passcode != nil else {
            completionHandler()
            return
        }

        var caller: String?

        let currentModule = String(describing: type(of: self))
        let separatorSet: CharacterSet = CharacterSet(charactersIn: " -[]+?.,")

        let stack = Thread.callStackSymbols
        for symbol in stack {
            let symbolComponents = symbol.components(separatedBy: separatorSet).filter { $0.count > 0 }

            // 0: stack, 1: framework, 2: memory address, 3: class caller, 4: method caller
            if symbolComponents.count < 5 {
                continue
            }

            if currentModule.hasPrefix(symbolComponents[1]) {
                continue
            }

            caller = symbolComponents[3] + "." + symbolComponents[4]
            break
        }

        guard let key = caller else {
            return
        }

        preparePasscodeWindow()
        presentWindow()

        let passcodeEntryViewController = window?.rootViewController as? PasscodeEntryViewController
        passcodeEntryViewController?.addCompletionHandler(forKey: key, handler: completionHandler)

        if UIApplication.shared.applicationState == .active {
            passcodeEntryViewController?.appear()
        }
    }
}
