//
//  PasscodeConstants.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit

@objc public enum PasscodeAppearance: Int {
    case light
    case dark
}

extension PasscodeAppearance {
    static var `default`: PasscodeAppearance {
        if #available (iOS 13.0, *), UITraitCollection.current.userInterfaceStyle == .dark {
            return .dark
        }
        return .light
    }
}

extension PasscodeAppearance {
    var foregroundColor: UIColor {
        switch self {
        case .light:
            return .black
        default:
            return .white
        }
    }

    var buttonForegroundColor: UIColor {
        switch self {
        case .light:
            return .black
        default:
            return .white
        }
    }

    var entryButtonBorderColor: UIColor {
        if #available(iOS 11, *) {
            return .clear
        } else {
            switch self {
            case .light:
                return .white
            default:
                return UIColor(white: 1.0, alpha: 0.3)
            }
        }
    }

    var setupButtonBackgroundColor: UIColor {
        switch self {
        case .light:
            return .white
        default:
            return UIColor(white: 1.0, alpha: 0.3)
        }
    }

    var setupButtonShadowColor: UIColor {
        if #available(iOS 11, *) {
            switch self {
            case .light:
                return UIColor(white: 0.0, alpha: 0.3)
            default:
                return UIColor(white: 0.0, alpha: 1.0)
            }
        }
        return .clear
    }

    var setupButtonShadowOffset: UIOffset {
        if #available(iOS 11, *) {
            return UIOffset(horizontal: 0.0, vertical: 1.0)
        }
        return .zero
    }

    var setupButtonHighlightedBackgroundColor: UIColor {
        switch self {
        case .light:
            return UIColor(white: 0.0, alpha: 0.1)
        default:
            return UIColor(white: 1.0, alpha: 0.1)
        }
    }

    var entryButtonBackgroundColor: UIColor {
        if #available(iOS 11, *) {
            switch self {
            case .light:
                return .white
            default:
                return .lightGray
            }
        } else {
            return .clear
        }
    }

    var entryButtonHighlightedBackgroundColor: UIColor {
        switch self {
        case .light:
            return .lightGray
        default:
            return .darkGray
        }
    }

    var blurEffect: UIBlurEffect {
        switch self {
        case .light:
            return UIBlurEffect(style: .extraLight)
        default:
            return UIBlurEffect(style: .dark)
        }
    }

    var statusBarStyle: UIStatusBarStyle {
        switch self {
        case .light:
            return .default
        default:
            return .lightContent
        }
    }
}
