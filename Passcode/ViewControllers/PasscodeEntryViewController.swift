//
//  PasscodeEntryViewController.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import UIKit
import LocalAuthentication

class PasscodeEntryViewController: PasscodeCoreViewController {
    @IBOutlet var titleImageView: UIImageView?

    private let authenticationContext = LAContext()

    var titleImage: UIImage? {
        didSet {
            titleImageView?.image = titleImage
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        modalPresentationStyle = .overFullScreen
        modalPresentationCapturesStatusBarAppearance = true

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        titleImageView?.image = titleImage
        titleImageView?.tintColor = appearance.foregroundColor
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIApplication.shared.waitForActiveApplicationState { [weak self] in
            self?.promptForBiometricVeritifaction()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return appearance.statusBarStyle
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            return .all
        }
        return [.portrait]
    }

    override var shouldAutorotate: Bool {
        return true
    }

    // MARK: handlers
    private var completionHandlers: [String: () -> Void] = [:]

    public func removeCompletionHandler(forKey key: String) {
        completionHandlers[key] = nil
    }

    public func addCompletionHandler(forKey key: String, handler: @escaping () -> Void) {
        completionHandlers[key] = handler
    }

    // MARK: notifications

    @objc func applicationWillEnterForeground(_ notification: Notification) {
        UIApplication.shared.waitForActiveApplicationState { [weak self] in
            self?.promptForBiometricVeritifaction()
        }
    }

    // MARK: lifecycle

    func successfulBiometricVeritifaction() {
        dotContainer?.arrangedSubviews.forEach { ($0 as? PasscodeDotView)?.state = .filled }
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300), execute: dismiss)
    }

    func promptForBiometricVeritifaction() {
        guard keychain.isBiometricVerificationEnabled, UIApplication.shared.applicationState == .active else {
            return
        }

        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: biometricVerificationReasonText) { [weak self] (success, error) in
            guard success else {
                return
            }

            DispatchQueue.main.async {
                self?.successfulBiometricVeritifaction()
            }
        }
    }

    override func completePasscodeEntry() {
        if validatePasscode() {
            dismiss()
        } else {
            resetPasscode(withShakeAnimation: true)
        }
    }

    func appear() {
        let animation: () -> Void
        if #available(iOS 11.0, *) {
            view.alpha = 0.0

            animation = { [weak self] in
                self?.view.alpha = 1.0
            }
        } else {
            let effect = backgroundView?.effect

            backgroundView?.effect = nil
            backgroundView?.contentView.alpha = 0.0

            animation = { [weak self] in
                self?.backgroundView?.contentView.alpha = 1.0
                self?.backgroundView?.effect = effect
            }
        }
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: animation, completion: nil)
    }

    override func dismiss() {
        let animation: () -> Void

        let handlers = completionHandlers
        let completion: (Bool) -> Void = { (position) in
            handlers.forEach { $1() }

            UIApplication.shared.delegate?.window??.makeKeyAndVisible()
        }

        if #available(iOS 11.0, *) {
            animation = { [weak self] in
                self?.view.alpha = 0.0
            }
        } else {
            animation = { [weak self] in
                self?.backgroundView?.contentView.alpha = 0.0
                self?.backgroundView?.effect = nil
            }
        }

        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: animation, completion: completion)
    }

    // MARK: view building

    override var buttonSpacing: UIOffset {
        return UIOffset(horizontal: 24.0, vertical: 8.0)
    }

    override func createDotView() -> PasscodeDotView? {
        return PasscodeEntryDotView()
    }

    override func createButton(withNumber number: Int) -> PasscodeButton {
        return PasscodeEntryButton(withNumber: number)
    }

    override func createButtonLabel(withNumber number: Int, text: String) -> PasscodeButtonLabel? {
        let label = PasscodeButtonLabel(withNumber: number, letters: text)
        label.numberFont = .systemFont(ofSize: 36.0)
        label.lettersFont = .boldSystemFont(ofSize: 9.0)

        return label
    }
}
