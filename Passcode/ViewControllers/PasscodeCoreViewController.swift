//
//  PasscodeCoreViewController.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 04/12/2017.
// 

import Foundation
import AudioToolbox

class PasscodeCoreViewController: UIViewController {
    @IBOutlet var labelContainer: UIStackView?
    @IBOutlet var dotContainer: UIStackView?
    @IBOutlet var buttonContainer: UIView?

    @IBOutlet var backgroundView: UIVisualEffectView?
    @IBOutlet var foregroundView: UIVisualEffectView?

    var appearance: PasscodeAppearance = .default
    var passcodeLength: Int = 4

    var keychain: Keychain = Keychain.shared

    var passcode: [Int] = [] {
        didSet {
            let currentLength = passcode.count
            dotContainer?.arrangedSubviews.enumerated().forEach { (index, dotView) in
                (dotView as? PasscodeDotView)?.state = index < currentLength ? .filled : .empty
            }

            if currentLength == passcodeLength {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: completePasscodeEntry)
            }
        }
    }

    // MARK: lifecycle

    func resetPasscode(withShakeAnimation shake: Bool) {
        passcode.removeAll()

        if shake {
            // damped sine wave oscillation
            let translations = [Int](0..<10).map { return 32 * exp(CGFloat(-$0) * 0.25) * cos(CGFloat.pi * CGFloat($0)) }
            let frameDuration = TimeInterval(1.0) / TimeInterval(translations.count)

            UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: [], animations: { [weak self] in
                for (index, offset) in translations.enumerated() {
                    UIView.addKeyframe(withRelativeStartTime: TimeInterval(index) * frameDuration, relativeDuration: frameDuration) {
                        self?.dotContainer?.transform = CGAffineTransform(translationX: offset, y: 0.0)
                    }
                }
            }, completion: { [weak self] (finished) in
                self?.view.isUserInteractionEnabled = true
            })

            // shake
            if #available(iOS 10.0, *) {
                let generator = UINotificationFeedbackGenerator()
                generator.prepare()
                generator.notificationOccurred(.error)

            } else {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
        }
    }

    func validatePasscode() -> Bool {
        guard let savedPasscode = keychain.passcode else {
            return true
        }
        return passcode == savedPasscode
    }

    func completePasscodeEntry() {
    }

    func dismiss() {
    }

    // MARK: view building

    func createDotView() -> PasscodeDotView? {
        return nil
    }

    func createButton(withNumber number: Int) -> PasscodeButton? {
        return nil
    }

    func createButtonLabel(withNumber number: Int, text: String) -> PasscodeButtonLabel? {
        return nil
    }

    func createDeleteButton() -> UIButton {
        let button = PasscodeButton(withNumber: 0)
        button.setImage(UIImage(eraseIconWithStrokeColor: appearance.foregroundColor), for: .normal)
        button.setImage(UIImage(highlightedEraseIconWithFillColor: appearance.foregroundColor), for: .highlighted)

        return button
    }

    var buttonSpacing: UIOffset {
        return .zero
    }

    var buttonMargins: UIEdgeInsets {
        return .zero
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView?.effect = appearance.blurEffect
        foregroundView?.effect = UIVibrancyEffect(blurEffect: appearance.blurEffect)

        dotContainer?.tintColor = appearance.foregroundColor

        labelContainer?.spacing = buttonSpacing.vertical
        labelContainer?.layoutMargins = buttonMargins

        if #available(iOS 11.0, *) {
            labelContainer?.isLayoutMarginsRelativeArrangement = true
        }

        setupButtons()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 13.0, *) {
            switch appearance {
            case .light:
                navigationController?.overrideUserInterfaceStyle = .light
            case .dark:
                navigationController?.overrideUserInterfaceStyle = .dark
            }
        }
    }

    private func setupButtons() {
        let layout = [
            [(1, " "), (2, "ABC"), (3, "DEF")],
            [(4, "GHI"), (5, "JKL"), (6, "MNO")],
            [(7, "PQRS"), (8, "TUV"), (9, "WXYZ")],
            [(0, "")]
        ]

        for row in layout {
            let stackView = UIStackView()
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.spacing = buttonSpacing.horizontal
            stackView.isUserInteractionEnabled = false

            labelContainer?.addArrangedSubview(stackView)

            for column in row {
                guard let button = createButton(withNumber: column.0), let label = createButtonLabel(withNumber: column.0, text: column.1) else {
                    continue
                }

                label.appearance = appearance
                stackView.addArrangedSubview(label)

                button.appearance = appearance
                button.translatesAutoresizingMaskIntoConstraints = false
                button.addTarget(self, action: #selector(addDigit(_:)), for: .touchUpInside)

                buttonContainer?.addSubview(button)
                button.leftAnchor.constraint(equalTo: label.leftAnchor).isActive = true
                button.topAnchor.constraint(equalTo: label.topAnchor).isActive = true
                button.rightAnchor.constraint(equalTo: label.rightAnchor).isActive = true
                button.bottomAnchor.constraint(equalTo: label.bottomAnchor).isActive = true
            }
        }

        let leftSpacer = UIView()
        let rightSpacer = UIView()

        let deleteButton = createDeleteButton()
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        deleteButton.addTarget(self, action: #selector(removeLastDigit), for: .touchUpInside)

        (labelContainer?.arrangedSubviews.last as? UIStackView)?.insertArrangedSubview(leftSpacer, at: 0)
        (labelContainer?.arrangedSubviews.last as? UIStackView)?.addArrangedSubview(rightSpacer)

        backgroundView?.contentView.addSubview(deleteButton)
        deleteButton.leftAnchor.constraint(equalTo: rightSpacer.leftAnchor).isActive = true
        deleteButton.topAnchor.constraint(equalTo: rightSpacer.topAnchor).isActive = true
        deleteButton.rightAnchor.constraint(equalTo: rightSpacer.rightAnchor).isActive = true
        deleteButton.bottomAnchor.constraint(equalTo: rightSpacer.bottomAnchor).isActive = true

        // dots
        for _ in 0..<passcodeLength {
            guard let dotView = createDotView() else {
                continue
            }

            dotContainer?.addArrangedSubview(dotView)
        }
    }

    @objc func removeLastDigit() {
        guard passcode.count > 0 else {
            return
        }

        passcode.removeLast()
    }

    @objc func addDigit(_ button: PasscodeButton) {
        guard passcode.count < passcodeLength else {
            return
        }

        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator(style: .medium).impactOccurred()
        }
        passcode.append(button.number)
    }
}
