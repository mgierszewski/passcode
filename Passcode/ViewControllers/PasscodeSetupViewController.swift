//
//  PasscodeSetupViewController.swift
//  Passcode
//
//  Created by Maciek Gierszewski on 19/12/2017.
// 

import Foundation

@objc public class PasscodeSetupNavigationBar: UINavigationBar {
}

enum SetupState {
    case initializing
    case enterOriginalPasscode
    case enterNewPasscode
    case confirmPasscode(_ passcodeToConfirm: [Int])
}

class PasscodeSetupViewController: PasscodeCoreViewController {
    @IBOutlet var contentView: UIView?
    @IBOutlet var headerLabel: UILabel?
    @IBOutlet var footerLabel: UILabel?
    @IBOutlet var errorLabel: UILabel?
    @IBOutlet var errorView: UIView?

    var foregroundColor: UIColor = .black
    var backgroundColor: UIColor = .groupTableViewBackground

    var setupNewPasscode: Bool = true
    var completionHandler: (Bool) -> Void = {_ in }

    var state: SetupState = .initializing

    var headerText: String? {
        get { return headerLabel?.text }
        set {
            headerLabel?.text = newValue
            headerLabel?.isHidden = (newValue ?? "").count == 0
        }
    }

    var footerText: String? {
        get { return footerLabel?.text }
        set {
            footerLabel?.text = newValue
            footerLabel?.isHidden = (newValue ?? "").count == 0
        }
    }

    var errorText: String? {
        get { return errorLabel?.text }
        set {
            errorLabel?.text = newValue
            errorView?.isHidden = (newValue ?? "").count == 0
        }
    }

    // MARK: view management

    override var buttonSpacing: UIOffset {
        if #available(iOS 11.0, *) {
            return UIOffset(horizontal: 6.0, vertical: 6.0)
        } else {
            return UIOffset(horizontal: 1.0, vertical: 1.0)
        }
    }

    override var buttonMargins: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return UIEdgeInsets(top: 6.0, left: 6.0, bottom: 6.0, right: 6.0)
        } else {
            return UIEdgeInsets.zero
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        preferredContentSize = CGSize(width: 320.0, height: 480.0)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = backgroundColor

        backgroundView?.backgroundColor = UIColor(red: 0/255, green: 10/255, blue: 30/255, alpha: 0.5)
        dotContainer?.tintColor = foregroundColor

        headerLabel?.textColor = foregroundColor
        headerLabel?.font = UIFont.preferredFont(forTextStyle: .body)

        footerLabel?.textColor = foregroundColor
        footerLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)

        errorLabel?.textColor = .white
        errorLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)

        errorView?.backgroundColor = .red
        errorView?.layoutMargins = UIEdgeInsets(top: 8.0, left: 16.0, bottom: 8.0, right: 16.0)

        if keychain.passcode != nil {
            title = setupNewPasscode ? changePasscodeText : deletePasscodeText
            setState(.enterOriginalPasscode)
        } else {
            title = setupPasscodeText
            setState(.enterNewPasscode)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        contentView?.layoutIfNeeded()
        errorView?.layer.cornerRadius = (errorView?.frame.height ?? 0.0) / 2.0
    }

    // MARK: lifecycle

    override func completePasscodeEntry() {
        switch state {
        case .enterOriginalPasscode:
            if validatePasscode() {
                if setupNewPasscode {
                    setState(.enterNewPasscode)
                } else {
                    keychain.passcode = nil
                    dismiss(withSuccess: true)
                }
            } else {
                resetPasscode(withShakeAnimation: true)
                errorText = incorrectPasscodeText
            }

        case .enterNewPasscode:
            setState(.confirmPasscode(passcode))

        case .confirmPasscode(let passcodeToConfirm):
            if passcode == passcodeToConfirm {
                keychain.passcode = passcode
                dismiss(withSuccess: true)
            } else {
                setState(.enterNewPasscode)
            }

        default:
            break
        }
    }

    func setState(_ newState: SetupState) {
        switch (state, newState) {
        case (_, .enterOriginalPasscode):
            resetContentView(withHeaderText: enterYourPasscodeText)

        case (.confirmPasscode, .enterNewPasscode):
            resetContentView(withHeaderText: enterPasscodeText, footerText: passcodesDoNotMatchText, transitionDirection: CATransitionSubtype.fromLeft.rawValue)

        case (.initializing, .enterNewPasscode):
            resetContentView(withHeaderText: enterPasscodeText)

        case (_, .enterNewPasscode):
            resetContentView(withHeaderText: enterPasscodeText, transitionDirection: CATransitionSubtype.fromRight.rawValue)

        case (_, .confirmPasscode):
            resetContentView(withHeaderText: confirmPasscodeText, transitionDirection: CATransitionSubtype.fromRight.rawValue)

        default:
            break
        }

        state = newState
    }

    func resetContentView(withHeaderText header: String? = nil, footerText footer: String? = nil, errorText error: String? = nil, transitionDirection: String? = nil) {
        headerText = header
        footerText = footer
        errorText = error
        passcode = []

        if let transitionSubtype = transitionDirection {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.subtype = convertToOptionalCATransitionSubtype(transitionSubtype)
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            transition.duration = 0.25
            contentView?.layer .add(transition, forKey: kCATransition)
        }
    }

    func dismiss(withSuccess success: Bool) {
        completionHandler(success)
        dismiss()
    }

    override func dismiss() {
        dismiss(animated: true, completion: nil)
    }

    // MARK: view creation

    override func createDotView() -> PasscodeDotView? {
        return PasscodeSetupDotView()
    }

    override func createButton(withNumber number: Int) -> PasscodeButton {
        return PasscodeSetupButton(withNumber: number)
    }

    override func createButtonLabel(withNumber number: Int, text: String) -> PasscodeButtonLabel? {
        let label = PasscodeButtonLabel(withNumber: number, letters: text)
        label.numberFont = .systemFont(ofSize: 25.0)
        label.lettersFont = .boldSystemFont(ofSize: 10.0)

        return label
    }

    // MARK: Actions

    @IBAction func cancel(withButton button: Any) {
        dismiss(withSuccess: false)
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToOptionalCATransitionSubtype(_ input: String?) -> CATransitionSubtype? {
	guard let input = input else { return nil }
	return CATransitionSubtype(rawValue: input)
}
