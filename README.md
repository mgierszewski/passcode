# README #

`Passcode` is a simple framework to provide passcode lock for your application. It's written in Swift 4.0 and it's available for iOS 10.0 or higher.

## Installation ##

Cocoapods:
`pod 'Passcode', :git => 'https://bitbucket.org/mgierszewski/passcode'`

Then just use `import Passcode` (Swift) or `#include <Passcode/Passcode.h>` (Objective-C).

## General ##

When created, passcode manager will automatically prompt when the app did finish launching or will enter foreground.
To create a passcode manager and perform the initial setup, just use `PasscodeManager.shared.applicationDidFinishLaunching()` in application delegate's `application:didFinishLaunchingWithOptions` method.

In order to prevent from being stuck in passcode lock screen after app reinstallation, during app's first run passcode manager will reset the passcode.


### How to setup (or change) a passcode ###
```
PasscodeManager.shared.setupPasscode(fromViewController sourceViewController: UIViewController, completionHandler: @escaping (Bool) -> Void)
```

### How to delete passcode ###
```
PasscodeManager.shared.deletePasscode(fromViewController sourceViewController: UIViewController, completionHandler: @escaping (Bool) -> Void)
```

### How to obtain available biometric verification method ###
```
let biometricVerificationType: BiometricVerificationType = PasscodeManager.shared.availableiometricVerificationType` // possible values are .faceID, .touchID or .none
```

### How to enable/disable biometric verification ###
```
PasscodeManager.shared.setBiometricVerificationEnabled(true, completionHandler: @escaping (enabled: Bool, error: Error?) -> Void)
```
or
```
PasscodeManager.shared.setBiometricVerificationEnabled(false, completionHandler: @escaping (enabled: Bool, error: Error?) -> Void)
```

### How to wait for passcode prompt to dismiss ###

For example, in `application:didFinishLaunchingWithOptions` or `applicationWillEnterForeground:` methods:
```
PasscodeManager.shared.waitForPasscode(withCompletionHandler completionHandler: @escaping () -> Void)
```

### How to setup a default passcode ###

Set
```
Passcode.shared.defaultPasscode: String?
```
if you would like to setup a default passcode before calling `PasscodeManager.shared.applicationDidFinishLaunching` method. The value should be a (preferably) 4-digit string and it should be set before the first launch of the app using the Passcode framework.

### Visual configuration ###

There are four (for now) properties in `PasscodeManager` that determine the look of passcode-related views:

The appearance of passcode entry keypad and passcode setup keyboard:
```
enum PasscodeAppearance: Int {
    case light
    case dark
}

var appearance: PasscodeAppearance = .light
```


The foreground (dots, labels) and background colors of passcode setup:
```
var backgroundColor: UIColor = .groupTableViewBackground
var foregroundColor: UIColor = .black
```


The title image in passcode entry:
```
var titleImage: UIImage?
```
